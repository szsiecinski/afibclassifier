import os
from domain.Signal import Signal


#Otwiera plik z próbkami sygnału i zapisuje próbki do obiektu klasy Signal
def open_from_file(filename, sampling_frequency=1, database=None, number=None):

    #inicjalizacja zmiennych
    signal = Signal(sampling_frequency)
    time = []
    lead1 = []
    lead2 = []

    with open(filename, encoding='utf-8') as opened_file:
        for line in opened_file:
            splitted = line.split("\t")
            time.append(float(splitted[0]))
            lead1.append(float(splitted[1]))
            lead2.append(float(splitted[2]))

    signal.time = time
    signal.lead1 = lead1
    signal.lead2 = lead2
    signal.database = database
    signal.number = number

    return signal


def _read_signal_files(path='data'):
    return [os.path.join(path, sigfile) for sigfile in os.listdir(path) if os.path.isfile(os.path.join(path, sigfile))]


def signals_generator(path='data'):
    sigfiles = _read_signal_files(path)

    for sigfile in sigfiles:
        fullname = sigfile
        name = sigfile.strip('.txt')
        els = os.path.split(name)
        name = els[1]
        db_n_no = name.split('-')
        db = db_n_no[0]

        if db == 'afdb':
            fs = 250
        if db == 'mitdb':
            fs = 360

        number = db_n_no[1]

        yield open_from_file(fullname, sampling_frequency=fs, database=db, number=number)


def signal_entries_generator(path='data'):
    # wczytaj nazwy plików
    files = [sigfile.strip('.txt') for sigfile in os.listdir(path) if os.path.isfile(os.path.join(path, sigfile))]
    # rozdziel nazwy baz danych od numerów wpisów
    dbs_n_nos = [file.split('-') for file in files]

    for db_n_no in dbs_n_nos:
        if db_n_no[0] == 'afdb':
            yield {'database': db_n_no[0], 'number': db_n_no[1], 'fs': 250}
        if db_n_no[0] == 'mitdb':
            yield {'database': db_n_no[0], 'number': db_n_no[1], 'fs': 360}


def read_all_signals():
    return list(signals_generator())