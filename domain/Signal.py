'''
Created on 9 mar 2016

@author: uzytkownik
'''
import scipy


class Signal:

    def __init__(self, fs=1):
        self.fs = fs
        self.time = []
        self.lead1 = []
        self.lead2 = []
        self.number = None
        self.database = None

    def __len__(self):
        return len(self.time)

    def get_sample(self, index):
        if 0 <= index < len(self.time):
            return self.time[index], self.lead1[index], self.lead2[index]
        else:
            raise Exception("Index out of range!")

    def get_range(self, begin, end):
        if 0 <= begin < end < len(self.time):
            slice_time = self.time[begin:end]
            slice_lead1 = self.lead1[begin:end]
            slice_lead2 = self.lead2[begin:end]
            slice_fs = self.fs
            slice = Signal(slice_fs)
            slice.time = slice_time
            slice.lead1 = slice_lead1
            slice.lead2 = slice_lead2
            return slice
        else:
            raise Exception("Range is invalid.")

    def split_lead1_into_chunks(self, n):
        return [self.lead1[i:i+n] for i in range(0, len(self.lead1), n)]

    def split_lead2_into_chunks(self, n):
        return [self.lead2[i:i+n] for i in range(0, len(self.lead2), n)]

    def filter(self, coefficients=399):
        tmpl1 = self.lead1
        tmpl2 = self.lead2

        h = scipy.signal.firwin(coefficients, [0.5, 45], pass_zero=False, window='blackman', nyq=self.fs)
        self.lead1 = scipy.signal.lfilter(h, 1, tmpl1)
        self.lead2 = scipy.signal.lfilter(h, 1, tmpl2)