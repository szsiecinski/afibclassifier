'''
Created on 7 mar 2016

@author: uzytkownik
'''

import numpy as npy
import scipy.signal as dsp

from domain.detect_peaks import detect_peaks
from .Signal import Signal
from .Tachogram import Tachogram


class QrsDetection:
    def __init__(self, signal=None):
        if signal is None:
            self.signal = Signal()
        else:
            self.signal = signal
        self.__qrs = []
    
    def filtering(self, signal):
        
        if isinstance(signal, Signal):
            coefficents = dsp.firwin(128, cutoff=[5, 15], window='boxcar', pass_zero=False, nyq=signal.fs)
            filteredOutput1 = dsp.lfilter(coefficents, 1, signal.lead1)
            filteredOutput2 = dsp.lfilter(coefficents, 1, signal.lead2)
            
            filteredSignal = Signal(signal.fs)
            filteredSignal.time = signal.time
            filteredSignal.lead1 = filteredOutput1
            filteredSignal.lead2 = filteredOutput2

            return filteredSignal
        else:
            raise Exception("Argument type exception!")

    '''
    Różniczkowanie numeryczne funkcji (wartości x i wartości y)
    @param x: argumenty
    @param y: wartości
    @return: pochodna funkcji 
    '''
    def diff(self, signal):
        
        if isinstance(signal, Signal):
            x = signal.time
            y = signal.lead1
            z = signal.lead2
            if len(x) and len(y) and len(z) > 0:
                dy = npy.diff(y, 1).tolist()
                dx = npy.diff(x, 1).tolist()
                dz = npy.diff(z, 1).tolist()

                dy.insert(0, (y[1]-y[0]))
                dx.insert(0, (x[1]-x[0]))
                dz.insert(0, (z[1]-z[0]))
                
                diffsignal = Signal(signal.fs)
                diffsignal.time = signal.time
                diffsignal.lead1 = npy.divide(dy,dx)
                diffsignal.lead2 = npy.divide(dz,dx)
                
                return diffsignal
            else:
                raise Exception("Signal has zero values!")
        else:
            raise Exception("Argument type exception!")

    '''
    Potęgowanie sygnału
    '''
    def square(self, signal):
        
        if isinstance(signal, Signal):
            
            ret = Signal(signal.fs)
            ret.time = signal.time
            lead1 = signal.lead1
            lead2 = signal.lead2

            lead1 = npy.power(lead1, 2)
            lead2 = npy.power(lead2, 2)

            ret.lead1 = npy.power(lead1, 2)
            ret.lead2 = npy.power(lead2, 2)

            return ret

        else:
            raise Exception("Argument type exception!")
        
    def smoothing(self, signal):
        
        if isinstance(signal, Signal):
            
            ret = Signal(signal.fs)
            ret.time = signal.time
            val1 = signal.lead1
            val2 = signal.lead2

            N = 0.15
            '''współczynniki filtru ruchomej średniej z pracy Pana i Tompkinsa'''
            smoothing_coeffs = npy.ones(round(N*self.signal.fs))/round(N*self.signal.fs)

            ret.lead1 = dsp.fftconvolve(val1, smoothing_coeffs, mode='same')
            ret.lead2 = dsp.fftconvolve(val2, smoothing_coeffs, mode='same')

            return ret

        else:
            raise Exception("Argument type exception!")
        
    def find_local_maxima(self, signal):
        if isinstance(signal, Signal):

            lead1 =  npy.array(signal.lead1)
            lead2 =  npy.array(signal.lead2)

            order=round(0.2*signal.fs)
            minimum_height1 = npy.mean(signal.lead1) / 8
            minimum_height2 = npy.mean(signal.lead2) / 8
            maxima1 = detect_peaks(lead1, mph=minimum_height1, mpd=order)
            maxima2 = detect_peaks(lead2, mph=minimum_height2, mpd=order)

            maxima = (maxima1, maxima2)

            return maxima

        else:
            raise Exception("Argument type exception!")

    def perform(self):
        filteredSignal = self.filtering(self.signal)
        diffSignal = self.diff(filteredSignal)
        squareSignal = self.square(diffSignal)
        smoothedSignal = self.smoothing(squareSignal)
        qrs_complexes = self.find_local_maxima(smoothedSignal)

        # zamiana próbek na sekundy
        self.__qrs = ([self.signal.time[q] for q in qrs_complexes[0]], [self.signal.time[q] for q in qrs_complexes[1]])

    def return_qrs(self):
        return self.__qrs

    def get_tachogram(self, lead=1, interpolate=False):
        """zależność długość zespołu RR od wystąpienia"""

        if lead == 1:
            rr = npy.diff(self.__qrs[0]).tolist()
        else:
            rr = npy.diff(self.__qrs[1]).tolist()

        rr.insert(0, 0)

        tachogram_time = npy.cumsum(rr)
        tachogram_time -= tachogram_time[0]

        t = Tachogram(time=tachogram_time, rr=rr)

        #czy interpolować
        if interpolate:
            t.interpolate(self.signal.time)

        return t
