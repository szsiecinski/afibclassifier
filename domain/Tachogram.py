'''
@author Szymon Sieciński
'''

import numpy
import scipy


class Tachogram:
    def __init__(self, rr=[], time=None):
        self.rr = rr
        self._interpolated = False
        if time is None:
            self.time = self._compute_rr_time()
        else:
            self.time = time

    def __len__(self):
        return len(self.rr)

    def _compute_rr_time(self):
        rr_time = numpy.cumsum(self.rr)
        rr_time -= rr_time[0]
        return rr_time

    def rr_median_filtration(self):
        rr = self.rr
        rr = scipy.signal.medfilt(rr, 5)
        self.rr = rr.tolist()

    def interpolate(self, signal_time):
        self._interpolated = True
        x = self.time
        y = self.rr

        ''' interpolacja liniowa '''
        interpolation = scipy.interpolate.interp1d(x, y, kind='linear', fill_value='extrapolate')
        self.rr = interpolation(signal_time)

    def get_mean_rr(self):
        return numpy.mean(self.rr)

    def get_drr(self):
        drr = numpy.diff(self.rr)
        drr = drr.tolist()
        drr.insert(0,0)
        return drr

    def get_scatter_plot(self):
        rr_plus_1 = self.rr[1:]
        rr = self.rr[:-1]
        lorenz_plot = ScatterPlot(rr, rr_plus_1)
        return lorenz_plot

    def split_rr_into_chunks(self, n, with_time=False):
        if with_time:
            rrs = [(self.rr[i:i+n], self.time[i:i + n]) for i in range(0, len(self.rr), n)]
        else:
            rrs = [self.rr[i:i+n] for i in range(0, len(self.rr), n)]
        return rrs

    def split_drr_into_chunks(self, n, with_time=False):
        drr = self.get_drr()

        if with_time:
            drrs = [(drr[i:i+n], self.time[i:i + n]) for i in range(0, len(drr), n)]
        else:
            drrs = [drr[i:i+n] for i in range(0, len(drr), n)]

        return drrs

    """Zmienna losowa x_i odstępów RR według Ghodrati i Marinello (2008)"""
    def get_xi(self, threshold=None):
        rr = self.rr

        mean_rr = [0] * len(rr)
        for i in range(1, len(rr)):
            mean_rr[i] = 0.9 * mean_rr[i-1] + 0.1 * rr[i]

        rr_minus_1 = rr[:-1]
        rr_minus_1 = numpy.insert(rr_minus_1, 0, 0)

        xi = [0] * len(rr)
        for i in range(0, len(rr)):
            if mean_rr[i] == 0:
                if threshold is not None:
                    if abs(rr[i] - rr_minus_1[i]) <= threshold:
                        xi[i] = rr[i] - rr_minus_1[i]
                    else:
                        xi[i] = 0
                else:
                    xi[i] = rr[i] - rr_minus_1[i]
            else:
                if threshold is not None:
                    if abs(rr[i] - rr_minus_1[i]) <= threshold:
                        xi[i] = rr[i] - rr_minus_1[i]/mean_rr[i]
                    else:
                        xi[i] = 0
                else:
                    xi[i] = rr[i] - rr_minus_1[i]/mean_rr[i]

        return xi

    def get_segmented_xi(self, seg_length, threshold=None, with_time=False):
        xis = self.get_xi(threshold=threshold)

        if with_time:
            ret = [(xis[i:i+seg_length], self.time[i:i + seg_length]) for i in range(0, len(xis), seg_length)]
        else:
            ret = [xis[i:i+seg_length] for i in range(0, len(xis), seg_length)]

        return ret