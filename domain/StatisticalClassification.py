import numpy
import scipy


class StatisticalClassifier:
    def __init__(self, tachogram=None, alpha=0.05):
        self.tachogram = tachogram
        self.alpha = alpha

    def _shapiro_test(self, x):
        D, p = scipy.stats.shapiro(x)
        if p > self.alpha:
            return 1
        else:
            return 0

    def classify(self, seg_width, signal_time=None):
        ints = self.tachogram.split_drr_into_chunks(seg_width, with_time=True)

        res = []

        for int in ints:
            drr = int[0]
            time = int[1]

            norm = self._shapiro_test(drr)
            val = [norm] * len(time)
            res += val

        if signal_time is not None:
            interpolator = scipy.interpolate.interp1d(self.tachogram.time, res, kind='linear', fill_value='extrapolate')
            res = interpolator(signal_time)

        return res