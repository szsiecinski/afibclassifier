from test import ClassifierTest
from test.ClassifierTest import *
from infrastructure import SignalReader
from domain import StatisticalClassification, QrsDetection, Tachogram

import numpy
import matplotlib.pyplot as plott

signals = {
    '201': None,
    '202': None,
    '205': None,
    '208': None,
    '210': None,
    '215': None,
    '217': None,
    '219': None,
    '220': None,
    '221': None
}

ref_values = {
    '201': None,
    '202': None,
    '205': None,
    '208': None,
    '210': None,
    '215': None,
    '217': None,
    '219': None,
    '220': None,
    '221': None
}

sig_len = 650000


def load_reference_episodes():
    #sygnał 201
    sig_201 = '201'
    ref_201 = [0] * sig_len

    ep1_range = ref_201[0:136225]
    ep2_range = ref_201[171814:185067]
    ep3_range = ref_201[574741:643410]

    ref_201[0:136225] = [1] * len(ep1_range)
    ref_201[171814:185067] = [1] * len(ep2_range)
    ref_201[574741:643410] = [1] * len(ep3_range)

    ref_values[sig_201] = ref_201

    #sygnał 202
    sig_202 = '202'
    ref_202 = [0] * sig_len

    ep1_range = ref_202[411337:416821]
    ep2_range = ref_202[423478:464993]
    ep3_range = ref_202[468677:549792]
    ep4_range = ref_202[468677:549792]

    ref_202[0:136225] = [1] * len(ep1_range)
    ref_202[171814:185067] = [1] * len(ep2_range)
    ref_202[574741:643410] = [1] * len(ep3_range)
    ref_202[567041:649999] = [1] * len(ep4_range)

    ref_values[sig_202] = ref_202

    #sygnał 205
    sig_205 = '205'
    ref_205 = [0] * sig_len

    ref_values[sig_205] = ref_205

    #sygnał 208
    sig_208 = '208'
    ref_208 = [0] * sig_len

    ref_values[sig_208] = ref_208

    #sygnał 210
    sig_210 = '210'
    ref_210 = [0] * sig_len

    ep1_range = ref_210[0:151553]
    ep2_range = ref_210[152487:216090]
    ep3_range = ref_210[218539:366153]
    ep4_range = ref_210[367319:368861]
    ep5_range = ref_210[370011:389829]
    ep6_range = ref_210[390957:392698]
    ep7_range = ref_210[394036:397019]
    ep8_range = ref_210[400543:596912]
    ep9_range = ref_210[598040:649999]

    ref_210[0:151553] = [1] * len(ep1_range)
    ref_210[152487:216090] = [1] * len(ep2_range)
    ref_210[218539:366153] = [1] * len(ep3_range)
    ref_210[367319:368861] = [1] * len(ep4_range)
    ref_210[370011:389829] = [1] * len(ep5_range)
    ref_210[390957:392698] = [1] * len(ep6_range)
    ref_210[394036:397019] = [1] * len(ep7_range)
    ref_210[400543:596912] = [1] * len(ep8_range)
    ref_210[598040:649999] = [1] * len(ep9_range)

    ref_values[sig_210] = ref_210

    #sygnał 215
    sig_215 = '215'
    ref_215 = [0] * sig_len

    ref_values[sig_215] = ref_215

    #sygnał 217
    sig_217 = '217'
    ref_217 = [0] * sig_len

    ep1_range = ref_217[67226:67965]
    ep2_range = ref_217[94357:95976]
    ep3_range = ref_217[157555:158716]
    ep4_range = ref_217[165651:167041]
    ep5_range = ref_217[284646:289434]
    ep6_range = ref_217[290617:295297]
    ep7_range = ref_217[298752:300577]
    ep8_range = ref_217[301962:305636]
    ep9_range = ref_217[307294:309351]
    ep10_range = ref_217[316517:321323]
    ep11_range = ref_217[326544:330617]
    ep12_range = ref_217[332902:341367]
    ep13_range = ref_217[342270:345942]
    ep14_range = ref_217[347565:349946]
    ep15_range = ref_217[350867:368755]
    ep16_range = ref_217[370853:376957]
    ep17_range = ref_217[378124:379456]
    ep18_range = ref_217[380914:386672]
    ep19_range = ref_217[391073:391905]
    ep20_range = ref_217[392817:395480]
    ep21_range = ref_217[431943:433087]
    ep22_range = ref_217[457930:459334]
    ep23_range = ref_217[483992:491497]

    ref_217[67226:67965] = [1] * len(ep1_range)
    ref_217[94357:95976] = [1] * len(ep2_range)
    ref_217[157555:158716] = [1] * len(ep3_range)
    ref_217[165652:167042] = [1] * len(ep4_range)
    ref_217[284646:289434] = [1] * len(ep5_range)
    ref_217[290617:295297] = [1] * len(ep6_range)
    ref_217[298752:300577] = [1] * len(ep7_range)
    ref_217[301962:305636] = [1] * len(ep8_range)
    ref_217[307294:309351] = [1] * len(ep9_range)
    ref_217[316517:321323] = [1] * len(ep10_range)
    ref_217[326544:330617] = [1] * len(ep11_range)
    ref_217[332902:341367] = [1] * len(ep11_range)
    ref_217[342270:345942] = [1] * len(ep12_range)
    ref_217[342270:345942] = [1] * len(ep13_range)
    ref_217[347565:349946] = [1] * len(ep14_range)
    ref_217[350867:368755] = [1] * len(ep15_range)
    ref_217[370853:376957] = [1] * len(ep16_range)
    ref_217[378124:379456] = [1] * len(ep17_range)
    ref_217[380914:386672] = [1] * len(ep18_range)
    ref_217[391073:391905] = [1] * len(ep19_range)
    ref_217[392817:395480] = [1] * len(ep20_range)
    ref_217[431943:433087] = [1] * len(ep21_range)
    ref_217[457930:459334] = [1] * len(ep22_range)
    ref_217[483992:491497] = [1] * len(ep23_range)

    ref_values[sig_217] = ref_217

    #sygnał 219
    sig_219 = '219'
    ref_219 = [0] * sig_len

    ep1_range = ref_219[0:33118]
    ep2_range = ref_219[36626:109191]
    ep3_range = ref_219[117598:300816]
    ep4_range = ref_219[307173:321701]
    ep5_range = ref_219[326614:331188]
    ep6_range = ref_219[339348:340198]
    ep7_range = ref_219[377624:381193]
    ep8_range = ref_219[401722:413165]
    ep9_range = ref_219[439100:480986]
    ep10_range = ref_219[501947:649999]

    ref_219[0:33118] = [1] * len(ep1_range)
    ref_219[36626:109191] = [1] * len(ep2_range)
    ref_219[117598:300816] = [1] * len(ep3_range)
    ref_219[307173:321701] = [1] * len(ep4_range)
    ref_219[326614:331188] = [1] * len(ep5_range)
    ref_219[339348:340198] = [1] * len(ep6_range)
    ref_219[377624:381193] = [1] * len(ep7_range)
    ref_219[401722:413165] = [1] * len(ep8_range)
    ref_219[439100:480986] = [1] * len(ep9_range)
    ref_219[501947:649999] = [1] * len(ep10_range)

    ref_values[sig_219] = ref_219

    #sygnał 220
    sig_220 = '220'
    ref_220 = [0] * sig_len

    ref_values[sig_220] = ref_220

    #sygnał 221
    sig_221 = '221'
    ref_221 = [0] * sig_len

    ep1_range = ref_221[0:103971]
    ep2_range = ref_221[105888:144439]
    ep3_range = ref_221[146217:196434]
    ep4_range = ref_221[198215:247715]
    ep5_range = ref_221[249462:253306]
    ep6_range = ref_221[255054:261160]
    ep7_range = ref_221[262902:272976]
    ep8_range = ref_221[274161:282508]
    ep9_range = ref_221[283149:303034]
    ep10_range = ref_221[303653:348018]
    ep11_range = ref_221[350700:364751]
    ep12_range = ref_221[366520:649999]

    ref_221[0:103971] = [1] * len(ep1_range)
    ref_221[105887:144439] = [1] * len(ep2_range)
    ref_221[146217:196434] = [1] * len(ep3_range)
    ref_221[198215:247715] = [1] * len(ep4_range)
    ref_221[249462:253306] = [1] * len(ep5_range)
    ref_221[255054:261160] = [1] * len(ep6_range)
    ref_221[262902:272976] = [1] * len(ep7_range)
    ref_221[274161:282508] = [1] * len(ep8_range)
    ref_221[283149:303034] = [1] * len(ep9_range)
    ref_221[303653:348018] = [1] * len(ep10_range)
    ref_221[350700:364751] = [1] * len(ep11_range)
    ref_221[366520:649999] = [1] * len(ep12_range)

    ref_values[sig_221] = ref_221


def drr_classification(signals, seg_width=50, alpha=0.01):
    tachograms = ClassifierTest.get_tachograms(signals)
    results = ClassifierTest.classify_by_drr(tachograms, signals, alpha=alpha, segment_width=seg_width)
    return results


def hht_classification(signals):
    pass


def drr_class_test(save_to_file=False, seg_width=50, alpha=0.01):
    ClassifierTest.load_signals(signals)
    load_reference_episodes()
    res1 = drr_classification(signals, seg_width=seg_width, alpha=alpha)

    for key, value in res1.items():
        tp = calculate_tp(res1[key], ref_values[key])
        tn = calculate_tn(res1[key], ref_values[key])
        fp = calculate_fp(res1[key], ref_values[key])
        fn = calculate_fn(res1[key], ref_values[key])

        se = calculate_sensitivity(tp, tn)
        sp = calculate_specificity(tn, fp)
        ppv = calculate_ppv(tp, fp)
        npv = calculate_npv(tn, fn)
        acc = calculate_accuracy(tp, tn, fp, fn)

        s_metoda = "Metoda: test rozkładu deltaRR\n"
        s_sygnal = "Sygnał %s \n" % key
        s_tp = "TP = %g\n" % tp
        s_tn = "TN = %g\n" % tn
        s_fp = "FP = %g\n" % fp
        s_fn = "FN = %g\n" % fn
        s_se = "Se = %g\n" % se
        s_sp = "Sp = %g\n" % sp
        s_ppv = "PPV = %g\n" % ppv
        s_npv = "NPV = %g\n" % npv
        s_acc = "Acc = %g\n\n" % acc

        if save_to_file:
            with open('report_mitdb_segment_{}_alpha_{}.txt'.format(seg_width, alpha), 'a') as report:
                report.write(s_metoda)
                report.write(s_sygnal)
                report.write(s_tp)
                report.write(s_tn)
                report.write(s_fp)
                report.write(s_fn)
                report.write(s_se)
                report.write(s_sp)
                report.write(s_ppv)
                report.write(s_npv)
                report.write(s_acc)
        else:
            print(s_metoda)
            print(s_sygnal)
            print(s_tp)
            print(s_tn)
            print(s_fp)
            print(s_fn)
            print(s_se)
            print(s_sp)
            print(s_ppv)
            print(s_npv)
            print(s_acc)


def perform_tests():
    drr_class_test(True, 50, 0.01)
    drr_class_test(True, 50, 0.05)
    drr_class_test(True, 50, 0.005)
    drr_class_test(True, 100, 0.01)
    drr_class_test(True, 25, 0.01)