import numpy

from test.ClassifierTest import *

signals = {
    '04015': None,
    '04043': None,
    '04048': None,
    '04126': None,
    '04746': None,
    '04936': None,
    '05091': None,
    '06453': None,
    '07162': None,
    '08215': None
}

ref_values = {
    '04015': None,
    '04043': None,
    '04048': None,
    '04126': None,
    '04746': None,
    '04936': None,
    '05091': None,
    '06453': None,
    '07162': None,
    '08215': None
}

sig_length = 9205760


def load_reference_episodes():
    sig = '04015'
    ref = [0] * sig_length

    ep1_len = len(ref[102583:119603])
    ep2_len = len(ref[121773:122194])
    ep3_len = len(ref[133347:166856])
    ep4_len = len(ref[1096244:1098053])
    ep5_len = len(ref[1135295:1139594])
    ep6_len = len(ref[1422435:1423547])
    ep7_len = len(ref[1459276:1460415])

    ref[102583:119603] = [1] * ep1_len
    ref[121773:122194] = [1] * ep2_len
    ref[133347:166856] = [1] * ep3_len
    ref[1096244:1098053] = [1] * ep4_len
    ref[1135295:1139594] = [1] * ep5_len
    ref[1422435:1423547] = [1] * ep6_len
    ref[1459276:1460415] = [1] * ep7_len

    ref_values[sig] = ref

    #04043
    sig = '04043'
    ref = [0] * sig_length

    ep1_len = len(ref[266497:376327])
    ep2_len = len(ref[2585283:2602515])
    ep3_len = len(ref[2634910:2739811])
    ep4_len = len(ref[2745161:2779580])
    ep5_len = len(ref[2834738:2841727])
    ep6_len = len(ref[3119234:3121705])
    ep7_len = len(ref[3324525:3352182])
    ep8_len = len(ref[3607123:3637518])
    ep9_len = len(ref[3665623:3680629])
    ep10_len = len(ref[3690355:3707031])
    ep11_len = len(ref[3714454:3729453])
    ep12_len = len(ref[3757892:3850172])
    ep13_len = len(ref[3864337:3882085])
    ep14_len = len(ref[3911964:3939728])
    ep15_len = len(ref[3947347:3992718])
    ep16_len = len(ref[4002975:4067052])
    ep17_len = len(ref[4100717:4135454])
    ep18_len = len(ref[4144612:4180312])
    ep19_len = len(ref[4184150:4207222])
    ep20_len = len(ref[4216964:4232560])
    ep21_len = len(ref[4397188:4429651])
    ep22_len = len(ref[4469007:4475225])
    ep23_len = len(ref[4484785:4508434])
    ep24_len = len(ref[4527257:4551867])
    ep25_len = len(ref[4572557:4602708])
    ep26_len = len(ref[4627076:4636997])
    ep27_len = len(ref[4650428:4700107])
    ep28_len = len(ref[4846372:4859019])
    ep29_len = len(ref[4977502:5001898])
    ep30_len = len(ref[5025435:5047414])
    ep31_len = len(ref[5064395:5076308])
    ep32_len = len(ref[5093222:5105439])
    ep33_len = len(ref[5122382:5136041])
    ep34_len = len(ref[5143705:5152530])
    ep35_len = len(ref[5223156:5237639])
    ep36_len = len(ref[5245919:5258588])
    ep37_len = len(ref[5269136:5310429])
    ep38_len = len(ref[5328716:5383210])
    ep39_len = len(ref[5412442:5446748])
    ep40_len = len(ref[5481761:5504397])
    ep41_len = len(ref[5518463:5535881])
    ep42_len = len(ref[5559926:5576036])
    ep43_len = len(ref[5592279:5633071])
    ep44_len = len(ref[5668366:5685491])
    ep45_len = len(ref[5713186:5719860])
    ep46_len = len(ref[5746977:5774761])
    ep47_len = len(ref[5797296:5822968])
    ep48_len = len(ref[5859229:5866171])
    ep49_len = len(ref[5891679:5931745])
    ep50_len = len(ref[6129597:6142318])
    ep51_len = len(ref[6167965:6200372])
    ep52_len = len(ref[6254933:6273885])
    ep53_len = len(ref[6378722:6404593])
    ep54_len = len(ref[6530474:6538180])
    ep55_len = len(ref[6562319:6574833])
    ep56_len = len(ref[6681067:6701452])
    ep57_len = len(ref[6749284:6762697])
    ep58_len = len(ref[6793369:6796578])
    ep59_len = len(ref[7083331:7099953])
    ep60_len = len(ref[7202179:7253975])
    ep61_len = len(ref[7312816:7338001])
    ep62_len = len(ref[7436857:7450330])
    ep63_len = len(ref[7471556:7477468])
    ep64_len = len(ref[7511861:7514585])
    ep65_len = len(ref[7537961:7555562])
    ep66_len = len(ref[7605109:7621969])
    ep67_len = len(ref[7737043:7769643])
    ep68_len = len(ref[8012815:8028177])
    ep69_len = len(ref[8048249:8049794])
    ep70_len = len(ref[8069400:8089734])
    ep71_len = len(ref[8198084:8218287])
    ep72_len = len(ref[8256561:8262298])
    ep73_len = len(ref[8355803:8362312])
    ep74_len = len(ref[8411313:8471312])
    ep75_len = len(ref[8693471:8707103])
    ep76_len = len(ref[8732494:8752735])
    ep77_len = len(ref[8788099:8801687])
    ep78_len = len(ref[8874297:8884574])
    ep79_len = len(ref[8910969:8921518])
    ep80_len = len(ref[8952010:8981285])

    ref[266497:376327] = [1] * ep1_len
    ref[2585283:2602515] = [1] * ep2_len
    ref[2634910:2739811] = [1] * ep3_len
    ref[2745161:2779580] = [1] * ep4_len
    ref[2834738:2841727] = [1] * ep5_len
    ref[3119234:3121705] = [1] * ep6_len
    ref[3324525:3352182] = [1] * ep7_len
    ref[3607123:3637518] = [1] * ep8_len
    ref[3665623:3680629] = [1] * ep9_len
    ref[3690355:3707031] = [1] * ep10_len
    ref[3714454:3729453] = [1] * ep11_len
    ref[3757892:3850172] = [1] * ep12_len
    ref[3864337:3882085] = [1] * ep13_len
    ref[3911964:3939728] = [1] * ep14_len
    ref[3947347:3992718] = [1] * ep15_len
    ref[4002975:4067052] = [1] * ep16_len
    ref[4100717:4135454] = [1] * ep17_len
    ref[4144612:4180312] = [1] * ep18_len
    ref[4184150:4207222] = [1] * ep19_len
    ref[4216964:4232560] = [1] * ep20_len
    ref[4397188:4429651] = [1] * ep21_len
    ref[4469007:4475225] = [1] * ep22_len
    ref[4484785:4508434] = [1] * ep23_len
    ref[4527257:4551867] = [1] * ep24_len
    ref[4572557:4602708] = [1] * ep25_len
    ref[4627076:4636997] = [1] * ep26_len
    ref[4650428:4700107] = [1] * ep27_len
    ref[4846372:4859019] = [1] * ep28_len
    ref[4977502:5001898] = [1] * ep29_len
    ref[5025435:5047414] = [1] * ep30_len
    ref[5064395:5076308] = [1] * ep31_len
    ref[5093222:5105439] = [1] * ep32_len
    ref[5122382:5136041] = [1] * ep33_len
    ref[5143705:5152530] = [1] * ep34_len
    ref[5223156:5237639] = [1] * ep35_len
    ref[5245919:5258588] = [1] * ep36_len
    ref[5269136:5310429] = [1] * ep37_len
    ref[5328716:5383210] = [1] * ep38_len
    ref[5412442:5446748] = [1] * ep39_len
    ref[5481761:5504397] = [1] * ep40_len
    ref[5518463:5535881] = [1] * ep41_len
    ref[5559926:5576036] = [1] * ep42_len
    ref[5592279:5633071] = [1] * ep43_len
    ref[5668366:5685491] = [1] * ep44_len
    ref[5713186:5719860] = [1] * ep45_len
    ref[5746977:5774761] = [1] * ep46_len
    ref[5797296:5822968] = [1] * ep47_len
    ref[5859229:5866171] = [1] * ep48_len
    ref[5891679:5931745] = [1] * ep49_len
    ref[6129597:6142318] = [1] * ep50_len
    ref[6167965:6200372] = [1] * ep51_len
    ref[6254933:6273885] = [1] * ep52_len
    ref[6378722:6404593] = [1] * ep53_len
    ref[6530474:6538180] = [1] * ep54_len
    ref[6562319:6574833] = [1] * ep55_len
    ref[6681067:6701452] = [1] * ep56_len
    ref[6749284:6762697] = [1] * ep57_len
    ref[6793369:6796578] = [1] * ep58_len
    ref[7083331:7099953] = [1] * ep59_len
    ref[7202179:7253975] = [1] * ep60_len
    ref[7312816:7338001] = [1] * ep61_len
    ref[7436857:7450330] = [1] * ep62_len
    ref[7471556:7477468] = [1] * ep63_len
    ref[7511861:7514585] = [1] * ep64_len
    ref[7537961:7555562] = [1] * ep65_len
    ref[7605109:7621969] = [1] * ep66_len
    ref[7737043:7769643] = [1] * ep67_len
    ref[8012815:8028177] = [1] * ep68_len
    ref[8048249:8049794] = [1] * ep69_len
    ref[8069400:8089734] = [1] * ep70_len
    ref[8198084:8218287] = [1] * ep71_len
    ref[8256561:8262298] = [1] * ep72_len
    ref[8355803:8362312] = [1] * ep73_len
    ref[8411313:8471312] = [1] * ep74_len
    ref[8693471:8707103] = [1] * ep75_len
    ref[8732494:8752735] = [1] * ep76_len
    ref[8788099:8801687] = [1] * ep77_len
    ref[8874297:8884574] = [1] * ep78_len
    ref[8910969:8921518] = [1] * ep79_len
    ref[8952010:8981285] = [1] * ep80_len

    ref_values[sig] = ref

    #04048
    sig = '04048'
    ref = [0] * sig_length

    ep1_len = len(ref[556676:580970])
    ep2_len = len(ref[716109:724075])
    ep3_len = len(ref[2148472:2153996])
    ep4_len = len(ref[2998790:3003177])
    ep5_len = len(ref[3638921:3677437])
    ep6_len = len(ref[4157976:4162993])
    ep7_len = len(ref[7681740:7686230])

    ref[556676:580970] = [1] * ep1_len
    ref[716109:724075] = [1] * ep2_len
    ref[2148472:2153996] = [1] * ep3_len
    ref[2998790:3003177] = [1] * ep4_len
    ref[3638921:3677437] = [1] * ep5_len
    ref[4157976:4162993] = [1] * ep6_len
    ref[7681740:7686230] = [1] * ep7_len

    ref_values[sig] = ref

    #04126
    sig = '04126'
    ref = [0] * sig_length

    ep1_len = len(ref[11387:138151])
    ep2_len = len(ref[171591:191327])
    ep3_len = len(ref[252785:322907])
    ep4_len = len(ref[748261:847440])
    ep5_len = len(ref[7832272:7836541])
    ep6_len = len(ref[8273013:8291854])
    ep7_len = len(ref[8307656:8313238])

    ref[11387:138151] = [1] * ep1_len
    ref[171591:191327] = [1] * ep2_len
    ref[252785:322907] = [1] * ep3_len
    ref[748261:847440] = [1] * ep4_len
    ref[7832272:7836541] = [1] * ep5_len
    ref[8273013:8291854] = [1] * ep6_len
    ref[8307656:8313238] = [1] * ep7_len

    ref_values[sig] = ref

    #04746
    sig = '04746'
    ref = [0] * sig_length

    ep1_len = len(ref[1097509:1098683])
    ep2_len = len(ref[2284260:2285671])
    ep3_len = len(ref[2319585:2326153])
    ep4_len = len(ref[2608375:5696114])
    ep5_len = len(ref[5705520:7496880])

    ref[1097509:1098683] = [1] * ep1_len
    ref[2284260:2285671] = [1] * ep2_len
    ref[2319585:2326153] = [1] * ep3_len
    ref[2608375:5696114] = [1] * ep4_len
    ref[5705520:7496880] = [1] * ep5_len

    ref_values[sig] = ref

    #04936
    sig = '04936'
    ref = [0] * sig_length

    ep1_len = len(ref[413690:447760])
    ep2_len = len(ref[1023429:1027875])
    ep3_len = len(ref[1091765:1105883])
    ep4_len = len(ref[1115501:1151701])
    ep5_len = len(ref[1174134:1189343])
    ep6_len = len(ref[1203804:1204430])
    ep7_len = len(ref[1205493:1316328])
    ep8_len = len(ref[1115501:1151701])
    ep9_len = len(ref[1174134:1189343])
    ep10_len = len(ref[1203804:1204430])
    ep11_len = len(ref[1205493:1316328])
    ep12_len = len(ref[1333474:1352711])
    ep13_len = len(ref[1364356:1382499])
    ep14_len = len(ref[1393566:1406690])
    ep15_len = len(ref[1430320:1668403])
    ep16_len = len(ref[1694047:1890995])
    ep17_len = len(ref[1904538:1905744])
    ep18_len = len(ref[1936942:2047169])
    ep19_len = len(ref[2068110:2102387])
    ep20_len = len(ref[2119394:2138827])
    ep21_len = len(ref[2147934:2280888])
    ep22_len = len(ref[2303974:2464066])
    ep23_len = len(ref[2494805:2498352])
    ep24_len = len(ref[2506460:3130657])
    ep25_len = len(ref[3165665:4753216])
    ep26_len = len(ref[4781739:4792438])
    ep27_len = len(ref[4804447:4901633])
    ep28_len = len(ref[4917776:5080722])
    ep29_len = len(ref[5100409:5110360])
    ep30_len = len(ref[5118730:5523272])
    ep31_len = len(ref[5548091:5687292])
    ep32_len = len(ref[5724605:5953275])
    ep33_len = len(ref[5981404:5984043])
    ep34_len = len(ref[5990300:5992458])
    ep35_len = len(ref[5995464:7325786])
    ep36_len = len(ref[7366278:7453004])
    ep37_len = len(ref[7490746:7582420])
    ep38_len = len(ref[7609873:7612043])
    ep39_len = len(ref[7619456:8269227])
    ep40_len = len(ref[8311033:8358030])

    ref[413690:447760] = [1] * ep1_len
    ref[121773:122194] = [1] * ep2_len
    ref[133347:166856] = [1] * ep3_len
    ref[1096244:1098053] = [1] * ep4_len
    ref[1135295:1139594] = [1] * ep5_len
    ref[1422435:1423547] = [1] * ep6_len
    ref[1459276:1460415] = [1] * ep7_len
    ref[1115501:1151701] = [1] * ep8_len
    ref[1174134:1189343] = [1] * ep9_len
    ref[1203804:1204430] = [1] * ep10_len
    ref[1205493:1316328] = [1] * ep11_len
    ref[1333474:1352711] = [1] * ep12_len
    ref[1364356:1382499] = [1] * ep13_len
    ref[1393566:1406690] = [1] * ep14_len
    ref[1430320:1668403] = [1] * ep15_len
    ref[1694047:1890995] = [1] * ep16_len
    ref[1904538:1905744] = [1] * ep17_len
    ref[1936942:2047169] = [1] * ep18_len
    ref[2068110:2102387] = [1] * ep19_len
    ref[2119394:2138827] = [1] * ep20_len
    ref[2147934:2280888] = [1] * ep21_len
    ref[2303974:2464066] = [1] * ep22_len
    ref[2494805:2498352] = [1] * ep23_len
    ref[2506460:3130657] = [1] * ep24_len
    ref[3165665:4753216] = [1] * ep25_len
    ref[4781739:4792438] = [1] * ep26_len
    ref[4804447:4901633] = [1] * ep27_len
    ref[4917776:5080722] = [1] * ep28_len
    ref[5100409:5110360] = [1] * ep29_len
    ref[5118730:5523272] = [1] * ep30_len
    ref[5548091:5687292] = [1] * ep31_len
    ref[5724605:5953275] = [1] * ep32_len
    ref[5981404:5984043] = [1] * ep33_len
    ref[5990300:5992458] = [1] * ep34_len
    ref[5995464:7325786] = [1] * ep35_len
    ref[7366278:7453004] = [1] * ep36_len
    ref[7490746:7582420] = [1] * ep37_len
    ref[7609873:7612043] = [1] * ep38_len
    ref[7619456:8269227] = [1] * ep39_len
    ref[8311033:8358030] = [1] * ep40_len

    ref_values[sig] = ref

    #05091
    sig = '05091'
    ref = [0] * sig_length

    ep1_len = len(ref[1633050:1633999])
    ep2_len = len(ref[1900119:1901524])
    ep3_len = len(ref[2270153:2270586])
    ep4_len = len(ref[2574097:2575189])
    ep5_len = len(ref[2591264:2593949])
    ep6_len = len(ref[2618883:2619972])
    ep7_len = len(ref[2877399:2880800])
    ep8_len = len(ref[4350348:4360986])

    ref[1633050:1633999] = [1] * ep1_len
    ref[1900119:1901524] = [1] * ep2_len
    ref[2270153:2270586] = [1] * ep3_len
    ref[2574097:2575189] = [1] * ep4_len
    ref[2591264:2593949] = [1] * ep5_len
    ref[2618883:2619972] = [1] * ep6_len
    ref[2877399:2880800] = [1] * ep7_len
    ref[4350348:4360986] = [1] * ep8_len

    ref_values[sig] = ref

    #06453
    sig = '06453'
    ref = [0] * sig_length

    ep1_len = len(ref[763936:767747])
    ep2_len = len(ref[926993:934173])
    ep3_len = len(ref[1046205:1063040])
    ep4_len = len(ref[1603834:1605896])
    ep5_len = len(ref[7326247:7364038])
    ep6_len = len(ref[7457732:7482874])

    ref[763936:767747] = [1] * ep1_len
    ref[926993:934173] = [1] * ep2_len
    ref[1046205:1063040] = [1] * ep3_len
    ref[1603834:1605896] = [1] * ep4_len
    ref[7326247:7364038] = [1] * ep5_len
    ref[7457732:7482874] = [1] * ep6_len

    ref_values[sig] = ref

    #07162
    sig = '07162'
    ref = [1] * sig_length

    ref_values[sig] = ref

    #08215
    sig = '08215'
    ref = [0] * sig_length

    ep1_len = len(ref[1774707:3204434])
    ep2_len = len(ref[3216994:sig_length-1])

    ref[102583:119603] = [1] * ep1_len
    ref[121773:122194] = [1] * ep2_len

    ref_values[sig] = ref


def drr_classification(signals, seg_width=50, alpha=0.01):
    tachograms = get_tachograms(signals)
    results = classify_by_drr(tachograms, signals, alpha=alpha, segment_width=seg_width)
    return results


def drr_class_test(save_to_file=False, seg_width=50, alpha=0.01):
    load_signals(signals)
    load_reference_episodes()
    res1 = drr_classification(signals, seg_width=seg_width, alpha=alpha)

    for key, value in res1.items():
        tp = calculate_tp(res1[key], ref_values[key])
        tn = calculate_tn(res1[key], ref_values[key])
        fp = calculate_fp(res1[key], ref_values[key])
        fn = calculate_fn(res1[key], ref_values[key])

        se = calculate_sensitivity(tp, tn)
        sp = calculate_specificity(tn, fp)
        ppv = calculate_ppv(tp, fp)
        npv = calculate_npv(tn, fn)
        acc = calculate_accuracy(tp, tn, fp, fn)

        s_metoda = "Metoda: test rozkładu deltaRR\n"
        s_sygnal = "Sygnał %s \n" % key
        s_tp = "TP = %g\n" % tp
        s_tn = "TN = %g\n" % tn
        s_fp = "FP = %g\n" % fp
        s_fn = "FN = %g\n" % fn
        s_se = "Se = %g\n" % se
        s_sp = "Sp = %g\n" % sp
        s_ppv = "PPV = %g\n" % ppv
        s_npv = "NPV = %g\n" % npv
        s_acc = "Acc = %g\n\n" % acc

        if save_to_file:
            with open('report_afdb_segment_{}_alpha_{}.txt'.format(seg_width, alpha), 'a') as report:
                report.write(s_metoda)
                report.write(s_sygnal)
                report.write(s_tp)
                report.write(s_tn)
                report.write(s_fp)
                report.write(s_fn)
                report.write(s_se)
                report.write(s_sp)
                report.write(s_ppv)
                report.write(s_npv)
                report.write(s_acc)
        else:
            print(s_metoda)
            print(s_sygnal)
            print(s_tp)
            print(s_tn)
            print(s_fp)
            print(s_fn)
            print(s_se)
            print(s_sp)
            print(s_ppv)
            print(s_npv)
            print(s_acc)

def perform_tests():
    drr_class_test(True, 50, 0.01)
    drr_class_test(True, 50, 0.05)
    drr_class_test(True, 50, 0.005)
    drr_class_test(True, 100, 0.01)
    drr_class_test(True, 25, 0.01)