from infrastructure import SignalReader
from domain.StatisticalClassification import StatisticalClassifier
from domain.QrsDetection import QrsDetection


def load_signals(signals_dict):
    signals = SignalReader.read_all_signals()

    for key, value in signals_dict.items():
        signal = [sig for sig in signals if sig.number == key]
        signal = signal[0]
        signals_dict[key] = signal


def get_tachograms(signals):
    load_signals(signals_dict=signals)
    tachograms = {}

    for key, value in signals.items():
        qrsDet = QrsDetection()
        qrsDet.signal = signals[key]
        qrsDet.perform()
        tachograms[key] = qrsDet.get_tachogram(interpolate=False)

    return tachograms


def classify_by_drr(tachograms, signals, alpha=0.01, segment_width=45):
    results = {}

    for key, value in tachograms.items():
        histogram_classifier = StatisticalClassifier(tachograms[key], alpha=alpha)
        results[key] = histogram_classifier.classify(seg_width=segment_width, signal_time=signals[key].time)

    return results


def count_nonzero_elements(el_list):
    l = [x for x in el_list if x != 0]
    return len(l)


def calculate_tp(results, reference):
    ret = [det and ref for det, ref in zip(results, reference)]
    return count_nonzero_elements(ret)


def calculate_fp(results, reference):
    ret = [det and (not ref) for det, ref in zip(results, reference)]
    return count_nonzero_elements(ret)


def calculate_tn(results, reference):
    ret = [(not det) and (not ref) for det, ref in zip(results, reference)]
    return count_nonzero_elements(ret)


def calculate_fn(results, reference):
    ret = [(not det) and ref for det, ref in zip(results, reference)]
    return count_nonzero_elements(ret)


def calculate_sensitivity(tp, fn):
    try:
        return tp/(tp+fn)
    except ZeroDivisionError:
        return 0


def calculate_specificity(tn, fp):
    try:
        return tn/(fp+tn)
    except ZeroDivisionError:
        return 0


def calculate_ppv(tp, fp):
    try:
        return tp/(tp+fp)
    except ZeroDivisionError:
        return 0


def calculate_npv(tn, fn):
    try:
        return tn/(tn+fn)
    except ZeroDivisionError:
        return 0


def calculate_accuracy(tp, tn, fp, fn):
    try:
        return (tp+tn)/(tp+tn+fp+fn)
    except ZeroDivisionError:
        return 0
