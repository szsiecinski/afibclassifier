from flask import Blueprint, Flask, jsonify

from application import SignalServices
from application.Services import service
from webui.webctrl import webui

app = Flask("afibclassifier")
app.register_blueprint(service, url_prefix='/api')
app.register_blueprint(webui)
