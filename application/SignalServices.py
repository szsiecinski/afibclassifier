import math

from flask import jsonify, request

from application.Services import service
from infrastructure import SignalReader

entries = []
signals = []


def read_all_signals():
    global entries, signals
    if len(entries) == 0 and len(signals) == 0:
        signals = list(SignalReader.signals_generator())
        entries = list(SignalReader.signal_entries_generator())


@service.route('/entries', methods=['GET'])
def get_entries():
    read_all_signals()
    return jsonify(entries)


@service.route('/entry/<path:id>', methods=['GET'])
def get_entry(id):
    read_all_signals()
    signal = [sig for sig in entries if sig['number'] == id]
    return jsonify(signal)


@service.route('/signal/<path:id>/length', methods=['GET'])
def get_signal_length(id):
    read_all_signals()
    signal = [sig for sig in signals if sig.number == id]
    signal = signal[0]
    return jsonify(len(signal.time))


@service.route('/signal/<path:id>/segment', methods=['GET'])
def get_signal_number_of_segments(id):

    read_all_signals()
    signal = [sig for sig in signals if sig.number == id]
    sig_len = len(signal.time)
    seg_len = int(request.args.get('length'))

    segments = math.ceil(sig_len/(seg_len * signal.fs))
    return jsonify(segments)


@service.route('/signal/<path:id>/slice', methods=['GET'])
def get_signal_slice(id):
    begin = int(request.args.get('begin'))
    end = int(request.args.get('end'))
    read_all_signals()

    signal = [sig for sig in signals if sig.number == id]
    signal = signal[0]
    slice = signal.get_range(begin, end)
    lead1 = slice.lead1
    lead2 = slice.lead2
    time = slice.time
    samples = [{'time': t, 'lead1': l1, 'lead2': l2} for t, l1, l2 in zip(time, lead1, lead2)]

    return jsonify(samples)