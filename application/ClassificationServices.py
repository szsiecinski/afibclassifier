from flask import jsonify, request
from werkzeug.contrib.cache import FileSystemCache

from application.Services import service
import application.SignalServices as signal_services

from domain.QrsDetection import QrsDetection
from domain.StatisticalClassification import StatisticalClassifier

cache = FileSystemCache('/')
cache_key = 'results'
timeout = 3600


def get_range(results, begin, end):
    if 0 <= begin < end < len(results):
        return results[begin:end]
    else:
        raise Exception("Range is invalid.")


@service.route('/classification/signal/<path:id>', methods=['POST'])
def classify(id):
    request_data = request.get_json(request.data)
    alpha = request_data['alpha']
    w = request_data['w']

    signal_services.read_all_signals()
    analyzedSignal = signal_services.get_signal(id)
    qrsDetection = QrsDetection(analyzedSignal)
    tachogram = qrsDetection.get_tachogram()
    classifier = StatisticalClassifier(tachogram, alpha=alpha)
    results = classifier.classify(w, signal_time=analyzedSignal.time)

    # zapis do bufora wyników
    cache.set(cache_key, results, 3600)


@service.route('/classification/results/slice', methods=['GET'])
def get_classified_data_slice():
    begin = int(request.args.get('begin'))
    end = int(request.args.get('end'))
    results = cache.get(cache_key)

    if results is None:
        return jsonify({
            "status": "signal not classified"
        })

    res_slice = get_range(results, begin, end)
    samples = [{'results': l1} for l1 in res_slice]

    return jsonify(samples)
