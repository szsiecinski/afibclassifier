/**
 * Kontroler strony głównej
 */
app.controller('HomeCtrl', ['$scope', '$http', '$state', function($scope, $http, $state) {

	$scope.signal = {
		selected: []
	};

	$http({url: '/api/entries', method: 'GET'}).then(function(response){
        $scope.signals = response.data;
	}, function(response){
	});

}]);