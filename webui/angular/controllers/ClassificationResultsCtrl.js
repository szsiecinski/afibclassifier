app.controller('ClassificationResultsCtrl', ['$scope', '$http', '$stateParams', '$state',
function($scope, $http, $stateParams, $state) {

    $scope.signalNumber = $stateParams.sigNo;
    $scope.database = $stateParams.db;

    $scope.segSize = $stateParams.w;
    $scope.alpha = $stateParams.alpha;

    $scope.back = function() {
        $state.go('signalInfo', {
            sigNo: $stateParams.sigNo,
            db: $stateParams.db
        });
    };

    $http({
        method: 'POST',
        url: '/api/classification/signal/' + $stateParams.sigNo,
        data: {
             w: $scope.segSize,
             alpha: $scope.alpha
        }
    }).then(function(success) {
        console.log(success);
    }, function(error) {
        console.log(error);
    });

    $http({method: 'GET', url: '/api/results/slice?begin='+ 0 + '&end=' + 3600}).then(function(r) {
        console.log(r);
    }, function(e) {
        console.log(e);
     });

}]);