/**
 * Kontroler informacji o sygnale
 */

app.controller('SignalInfoCtrl', ['$scope', '$http', '$stateParams', '$state',
    function($scope, $http, $stateParams, $state) {

	$scope.signalNumber = $stateParams.sigNo;
    $scope.database = $stateParams.db;

    $scope.segSize = 50;
    $scope.alpha=0.01;

    //dodaj przeliczenie na 20s (z uwzględnieniem fs)
    var BEGIN = 0, segment = 7200, END=Infinity;
    $scope.MAX = segment;
    var left = BEGIN;

    $http({url: '/api/signal/' + $scope.signalNumber + '/length', method: 'GET'})
    .then(function(success){ $scope.MAX = success.data; END = $scope.MAX; },
    function(fail){
        console.log(fail);
    })

    var right = segment;

    $scope.begin = left == BEGIN;
    $scope.end = right == END;

    $scope.signal_back = function() {
        right -= segment;

        if(left >= BEGIN)
            left -= segment;
        else
            left = BEGIN;

        $scope.begin = left == BEGIN;
        $scope.end = right == END;

        $scope.drawECG();
    };

    $scope.signal_forward = function() {
        left += segment;

        if(right <= END)
            right += segment;
        else
            right = END;

        $scope.begin = left == BEGIN;
        $scope.end = right == END;

        $scope.drawECG();
    };

    $scope.drawECG = function drawFn() {

        $http({url: '/api/signal/' + $scope.signalNumber + '/slice?begin='+ left + '&end=' + right, method: 'GET'})
        .then(function(response) {
            var leads = response.data;

            $scope.chartOptions = {
                margin: {top: 5, left: 5, bottom: 5, right: 5},
                drawLegend: false,
                grid: {
                  x: true,
                  y: true
                },

                series: [{
                    axis: "y",
                    dataset: "leads",
                    type: ['line'],
                    color: "red",
                    visible: "true",
                    label: "Odprowadzenie 1",
                    key: "lead1",
                    id: "lead1"
                },
                {
                    axis: "y",
                    dataset: "leads",
                    type: ['line'],
                    color: "black",
                    visible: "false",
                    label: "Odprowadzenie 2",
                    key: "lead2",
                    id: "lead2"
                }],
                axes: {
                    x: {key: "time"}
                }
            };

            $scope.signalData = {
                leads: leads
            };

        }, function(response){
            console.log(response)
        });
    };
    $scope.drawECG();

    $scope.classify = function classifyFn() {

        var segmentLength = $scope.segSize;
        var alpha = $scope.alpha;

        $state.go('classificationResults', {
            db: $stateParams.db,
            sigNo: $stateParams.sigNo,
            w: segmentLength,
            alpha: alpha
        });

    };
}]);