/**
 * Plik główny AngularJS
 */
'use strict';

var app = angular.module('afibclassifier', ['ngSanitize', 'ngAnimate', 'ui.bootstrap', 'ui.select', 'ui.router',
'angular-loading-bar', 'n3-line-chart']);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('home', {
			url: '/',
			controller: 'HomeCtrl',
			templateUrl: 'partials/HomeView.html'
		})
		.state('signalInfo', {
			url: '/signal/:db/:sigNo',
			controller: 'SignalInfoCtrl',
			templateUrl: 'partials/SignalInfoView.html'
		})
		.state('classificationResults', {
		    url: '/signal/:db/:sigNo/classification/:w/:alpha',
		    controller: 'ClassificationResultsCtrl',
		    templateUrl: 'partials/ClassificationResultsView.html'
		});
}]);