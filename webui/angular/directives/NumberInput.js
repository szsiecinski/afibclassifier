'use strict';

app.directive('numberInput', function() {
    return {
        restrict: 'EA',
        template: '<div class="input-group">'+
            '<input type="text" class="form-control" name="{{name || \'numberInput\'}}" placeholder="placeholder" data-ng-model="value" data-ng-pattern="pattern">' +
            '<div class="input-group-btn">' +
                '<button class="btn btn-primary" type="button" data-ng-click="plus()"><span class="glyphicon glyphicon-plus"></span></button>'+
                '<button class="btn btn-primary" type="button" data-ng-click="minus()"><span class="glyphicon glyphicon-minus"></span></button>'+
            '</div>' +
        '</div>',
        scope: {
            value: '=',
            min: '=',
            max: '=',
            step: '=?'
        },
        controller: ['$scope', function($scope) {
            $scope.pattern = /^\d+(\.\d+)?$/;

            $scope.plus = function() {
                if($scope.value <= $scope.max)
                $scope.value += $scope.step;
            };
            $scope.minus = function() {
                if($scope.value > $scope.min)
                $scope.value -= $scope.step;
            };
        }]
    };
});