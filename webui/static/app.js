/**
 * Plik główny AngularJS
 */
'use strict';

var app = angular.module('afibclassifier', ['ngSanitize', 'ngAnimate', 'ui.bootstrap', 'ui.select', 'ui.router',
'angular-loading-bar', 'n3-line-chart']);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('home', {
			url: '/',
			controller: 'HomeCtrl',
			templateUrl: 'partials/HomeView.html'
		})
		.state('signalInfo', {
			url: '/signal/:db/:sigNo',
			controller: 'SignalInfoCtrl',
			templateUrl: 'partials/SignalInfoView.html'
		})
		.state('classificationResults', {
		    url: '/signal/:db/:sigNo/classification/:w/:alpha',
		    controller: 'ClassificationResultsCtrl',
		    templateUrl: 'partials/ClassificationResultsView.html'
		});
}]);
app.controller('ClassificationResultsCtrl', ['$scope', '$http', '$stateParams', '$state',
function($scope, $http, $stateParams, $state) {

    $scope.signalNumber = $stateParams.sigNo;
    $scope.database = $stateParams.db;

    $scope.segSize = $stateParams.w;
    $scope.alpha = $stateParams.alpha;

    $scope.back = function() {
        $state.go('signalInfo', {
            sigNo: $stateParams.sigNo,
            db: $stateParams.db
        });
    };

    $http({
        method: 'POST',
        url: '/api/classification/signal/' + $stateParams.sigNo,
        data: {
             w: $scope.segSize,
             alpha: $scope.alpha
        }
    }).then(function(success) {
        console.log(success);
    }, function(error) {
        console.log(error);
    });

    $http({method: 'GET', url: '/api/results/slice?begin='+ 0 + '&end=' + 3600}).then(function(r) {
        console.log(r);
    }, function(e) {
        console.log(e);
     });

}]);
/**
 * Kontroler strony głównej
 */
app.controller('HomeCtrl', ['$scope', '$http', '$state', function($scope, $http, $state) {

	$scope.signal = {
		selected: []
	};

	$http({url: '/api/entries', method: 'GET'}).then(function(response){
        $scope.signals = response.data;
	}, function(response){
	});

}]);
/**
 * Kontroler informacji o sygnale
 */

app.controller('SignalInfoCtrl', ['$scope', '$http', '$stateParams', '$state',
    function($scope, $http, $stateParams, $state) {

	$scope.signalNumber = $stateParams.sigNo;
    $scope.database = $stateParams.db;

    $scope.segSize = 50;
    $scope.alpha=0.01;

    //dodaj przeliczenie na 20s (z uwzględnieniem fs)
    var BEGIN = 0, segment = 7200, END=Infinity;
    $scope.MAX = segment;
    var left = BEGIN;

    $http({url: '/api/signal/' + $scope.signalNumber + '/length', method: 'GET'})
    .then(function(success){ $scope.MAX = success.data; END = $scope.MAX; },
    function(fail){
        console.log(fail);
    })

    var right = segment;

    $scope.begin = left == BEGIN;
    $scope.end = right == END;

    $scope.signal_back = function() {
        right -= segment;

        if(left >= BEGIN)
            left -= segment;
        else
            left = BEGIN;

        $scope.begin = left == BEGIN;
        $scope.end = right == END;

        $scope.drawECG();
    };

    $scope.signal_forward = function() {
        left += segment;

        if(right <= END)
            right += segment;
        else
            right = END;

        $scope.begin = left == BEGIN;
        $scope.end = right == END;

        $scope.drawECG();
    };

    $scope.drawECG = function drawFn() {

        $http({url: '/api/signal/' + $scope.signalNumber + '/slice?begin='+ left + '&end=' + right, method: 'GET'})
        .then(function(response) {
            var leads = response.data;

            $scope.chartOptions = {
                margin: {top: 5, left: 5, bottom: 5, right: 5},
                drawLegend: false,
                grid: {
                  x: true,
                  y: true
                },

                series: [{
                    axis: "y",
                    dataset: "leads",
                    type: ['line'],
                    color: "red",
                    visible: "true",
                    label: "Odprowadzenie 1",
                    key: "lead1",
                    id: "lead1"
                },
                {
                    axis: "y",
                    dataset: "leads",
                    type: ['line'],
                    color: "black",
                    visible: "false",
                    label: "Odprowadzenie 2",
                    key: "lead2",
                    id: "lead2"
                }],
                axes: {
                    x: {key: "time"}
                }
            };

            $scope.signalData = {
                leads: leads
            };

        }, function(response){
            console.log(response)
        });
    };
    $scope.drawECG();

    $scope.classify = function classifyFn() {

        var segmentLength = $scope.segSize;
        var alpha = $scope.alpha;

        $state.go('classificationResults', {
            db: $stateParams.db,
            sigNo: $stateParams.sigNo,
            w: segmentLength,
            alpha: alpha
        });

    };
}]);
'use strict';

app.directive('numberInput', function() {
    return {
        restrict: 'EA',
        template: '<div class="input-group">'+
            '<input type="text" class="form-control" name="{{name || \'numberInput\'}}" placeholder="placeholder" data-ng-model="value" data-ng-pattern="pattern">' +
            '<div class="input-group-btn">' +
                '<button class="btn btn-primary" type="button" data-ng-click="plus()"><span class="glyphicon glyphicon-plus"></span></button>'+
                '<button class="btn btn-primary" type="button" data-ng-click="minus()"><span class="glyphicon glyphicon-minus"></span></button>'+
            '</div>' +
        '</div>',
        scope: {
            value: '=',
            min: '=',
            max: '=',
            step: '=?'
        },
        controller: ['$scope', function($scope) {
            $scope.pattern = /^\d+(\.\d+)?$/;

            $scope.plus = function() {
                if($scope.value <= $scope.max)
                $scope.value += $scope.step;
            };
            $scope.minus = function() {
                if($scope.value > $scope.min)
                $scope.value -= $scope.step;
            };
        }]
    };
});