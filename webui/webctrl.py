from flask import Blueprint, send_from_directory, safe_join

webui = Blueprint('webui', __name__, static_folder='static', template_folder='')
WEBUI_PATH = 'webui'


@webui.route("/")
def index():
    return send_from_directory(WEBUI_PATH, 'index.html')


@webui.route('/<path:path>')
def serve_file(path):
    return send_from_directory(safe_join(WEBUI_PATH, 'static'), path)


@webui.route('/partials/<path:path>')
def serve_partial(path):
    return send_from_directory(safe_join(WEBUI_PATH, 'static/partials'), path)


@webui.errorhandler(404)
def not_found(*args):
    return send_from_directory(WEBUI_PATH, 'error_404.html')