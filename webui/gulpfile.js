var gulp = require('gulp');
var concat = require('gulp-concat');
var del = require('del');
var uglify = require('gulp-uglify');

gulp.task('css', function() {
    return gulp.src(['node_modules/bootstrap/dist/css/*.min.css', 'node_modules/angular/*.css',
    'node_modules/angular-ui-bootstrap/bootstrap-theme.css',
    'node_modules/angular-ui-bootstrap/bootstrap.css',
    'node_modules/ui-select/dist/select.min.css',
    'node_modules/angular-loading-bar/build/loading-bar.css',
    'node_modules/n3-charts/build/LineChart.min.css',
    'styles.css'])
    .pipe(concat('styles.css'))
    .pipe(gulp.dest('static'));
});

gulp.task('fonts', function() {
    return gulp.src('node_modules/bootstrap/fonts/*')
    .pipe(gulp.dest('static/fonts'));
});

gulp.task('angular', function() {
    return gulp.src(['node_modules/angular/angular.js',
    'node_modules/angular-animate/angular-animate.js',
    'node_modules/angular-sanitize/angular-sanitize.js',
    'node_modules/angular-ui-bootstrap/dist/*.js',
    'node_modules/angular-ui-router/release/*.min.js',
    'node_modules/angular-loading-bar/build/loading-bar.js',
    'node_modules/ui-select/dist/select.js',
    'node_modules/n3-charts/build/LineChart.min.js'])
    .pipe(concat('angular.js'))
    .pipe(gulp.dest('static'));
});

gulp.task('libs', function() {
    return gulp.src(['node_modules/jquery/dist/*.min.js',
    'node_modules/bootstrap/dist/js/bootstrap.js',
    'node_modules/d3/d3.min.js'])
    .pipe(concat('libs.js'))
    .pipe(gulp.dest('static'));
});

gulp.task('partials', function() {
    return gulp.src('angular/partials/*')
    .pipe(gulp.dest('static/partials'));
});

gulp.task('app', ['partials'], function() {
    return gulp.src('angular/**/*.js')
    .pipe(concat('app.js'))
    .pipe(gulp.dest('static'));
});

gulp.task('clear', function() {
    return del(['static/*']);
});

gulp.task('watch', ['bundle'], function() {
    gulp.watch('angular/**/*.js', ['bundle']);
});

gulp.task('bundle', ['libs', 'css', 'fonts', 'angular', 'app'])

gulp.task('default', ['clear', 'bundle']);