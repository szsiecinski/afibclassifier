AFibClassifier v 0.2.0

Klasyfikator migotania przedsionków

# Stos technologiczny:
* Python 3.5.2
* Flask 0.11
* HTML5
* CSS3
* AngularJS 1.5

# Budowanie projektu:
* Wymagane są:
    * Python 3
    * NumPy i SciPy
    * Flask 0.11
    * Edytor tekstu lub IDE dla Pythona
    * Node.js (do uruchomienia NPM)
    * NPM (do zarządzania zależnościami front-end)
    * Gulp (do scalania skryptów JS i plików CSS)

* Przebieg budowania: 
    1. npm install
    1. gulp
    1. runner.py 

# Struktura:
* domain - domena (algorytmy, typy danych)
* infrastructure - dostęp do danych sygnałowych (tutaj z plików tekstowych)
* application - warstwa spinająca domenę, infrastrukturę, UI
* webui - webowy interfejs użytkownika
* test - testy klasyfikatora