#! /usr/bin/python3

from application.AppConfig import app
from test import MitdbTest, AfdbTest

# print('testy MITDB')
# MitdbTest.perform_tests()
# print('testy AFDB')
# AfdbTest.perform_tests()

address = '0.0.0.0'
port = 8080
debug_mode = True

'''Uruchamianie aplikacji'''
app.run(address, port=port, debug=debug_mode)